<?php

namespace MD\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function index1Action()
    {
        return $this->render('MDMainBundle:Default:index.html.twig');
    }
    
    public function index2Action()
    {
        return $this->render('MDMainBundle:Default:index2.html.twig');
    }
    
    public function index3Action()
    {
        return $this->render('MDMainBundle:Default:index3.html.twig');
    }
}
